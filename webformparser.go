// Copyright 2023 JakobDev. All rights reserved.
// SPDX-License-Identifier: MIT

package webformparser

import (
	"errors"
	"fmt"
	"mime/multipart"
	"net/http"
	"reflect"
	"strconv"
	"strings"
	"time"
)

type ErrParseStructTag struct {
	TagPart string
	Text    string
}

func (err ErrParseStructTag) Error() string {
	return fmt.Sprintf("Error parsing Tag %s: %s", err.TagPart, err.Text)
}

// IsErrParseStructTag returns if the error is a ErrParseStructTag
func IsErrParseStructTag(err error) bool {
	_, ok := err.(ErrParseStructTag)
	return ok
}

// Hallo
type ErrParseValue struct {
	StructName string
	HtmlName   string
	Value      string
	ParseError error
}

func (err ErrParseValue) Error() string {
	return fmt.Sprintf("Error parsing %s with value %s: %v", err.HtmlName, err.Value, err.ParseError)
}

// IsErrParseValue returns if the error is a ErrParseValue
func IsErrParseValue(err error) bool {
	_, ok := err.(ErrParseValue)
	return ok
}

type ErrUnsupportedType struct {
	FieldName string
}

func (err ErrUnsupportedType) Error() string {
	return fmt.Sprintf("%s has a unsupported type", err.FieldName)
}

// IsErrUnsupportedType returns if the error is a ErrUnsupportedType
func IsErrUnsupportedType(err error) bool {
	_, ok := err.(ErrUnsupportedType)
	return ok
}

type timeFormat int8

const (
	timeFormatDatetime timeFormat = iota
	timeFormatDate
	timeFormatTime
	timeFormatWeek
)

type structTagInfo struct {
	Name       string
	TimeFormat timeFormat
	Ignore     bool
}

func parseStructTag(tag string, fieldName string) (*structTagInfo, error) {
	tagInfo := new(structTagInfo)
	tagInfo.Name = fieldName
	tagInfo.TimeFormat = timeFormatDatetime

	if tag == "" {
		return tagInfo, nil
	}

	for _, tagPart := range strings.Fields(tag) {
		parts := strings.Split(tagPart, ":")
		switch strings.ToLower(parts[0]) {
		case "name":
			if len(parts) == 1 {
				return nil, ErrParseStructTag{TagPart: parts[0], Text: "Invalid syntax. Expected name:<name>"}
			}

			tagInfo.Name = parts[1]
		case "timeformat":
			if len(parts) == 1 {
				return nil, ErrParseStructTag{TagPart: parts[0], Text: "Invalid syntax. Expected timeformat:<format>"}
			}

			switch strings.ToLower(parts[1]) {
			case "datetime":
				tagInfo.TimeFormat = timeFormatDatetime
			case "date":
				tagInfo.TimeFormat = timeFormatDate
			case "time":
				tagInfo.TimeFormat = timeFormatTime
			case "week":
				tagInfo.TimeFormat = timeFormatWeek
			default:
				return nil, ErrParseStructTag{TagPart: parts[0], Text: "Invalid format. Expected datetime, date, time or week"}
			}
		case "ignore":
			tagInfo.Ignore = true
		}
	}

	return tagInfo, nil
}

func hasFormKey(r *http.Request, key string) bool {
	if r.Form.Has(key) {
		return true
	}

	_, _, err := r.FormFile(key)
	if err == nil || !errors.Is(err, http.ErrMissingFile) {
		return true
	}

	return false
}

func parseWeekTime(weekString string) (*time.Time, error) {
	parts := strings.Split(weekString, "-W")
	if len(parts) != 2 {
		return nil, fmt.Errorf("inavlid week string")
	}

	year, err := strconv.Atoi(parts[0])
	if err != nil {
		return nil, err
	}

	week, err := strconv.Atoi(parts[1])
	if err != nil {
		return nil, err
	}

	currentTime := time.Date(year, 0, 0, 0, 0, 0, 0, time.Local)

	for {
		_, currentWeek := currentTime.ISOWeek()
		if currentWeek == week {
			break
		}

		currentTime = currentTime.AddDate(0, 0, 1)
	}

	return &currentTime, nil
}

func ParseForm[T interface{}](form T, r *http.Request) (*T, error) {
	formType := reflect.TypeOf(form)
	for i := 0; i < formType.NumField(); i++ {
		fieldName := formType.Field(i).Name
		value := reflect.Indirect(reflect.ValueOf(&form))
		field := value.FieldByName(fieldName)

		structField, found := formType.FieldByName(fieldName)
		if !found {
			continue
		}

		tagInfo, err := parseStructTag(structField.Tag.Get("webformparser"), fieldName)
		if err != nil {
			return nil, err
		}

		if tagInfo.Ignore {
			continue
		}

		currentValue := r.FormValue(tagInfo.Name)

		if !hasFormKey(r, tagInfo.Name) {
			continue
		}

		// Init the ErrParsevalue, so we only need to set the ParseError later
		parseValueError := ErrParseValue{StructName: fieldName, HtmlName: tagInfo.Name, Value: currentValue}

		switch field.Type().Kind() {
		case reflect.String:
			field.SetString(currentValue)
			continue
		case reflect.Bool:
			switch currentValue {
			case "on":
				field.SetBool(true)
				continue
			case "off", "":
				field.SetBool(false)
				continue
			default:
				parseValueError.ParseError = fmt.Errorf("expected on or off")
				return nil, parseValueError
			}
		case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
			integer, err := strconv.ParseInt(currentValue, 10, 64)
			if err != nil {
				parseValueError.ParseError = err
				return nil, parseValueError
			}

			field.SetInt(integer)
			continue
		case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
			integer, err := strconv.ParseUint(currentValue, 10, 64)
			if err != nil {
				parseValueError.ParseError = err
				return nil, parseValueError
			}

			field.SetUint(integer)
			continue
		case reflect.Float32, reflect.Float64:
			number, err := strconv.ParseFloat(currentValue, 64)
			if err != nil {
				parseValueError.ParseError = err
				return nil, parseValueError
			}

			field.SetFloat(number)
			continue
		}

		switch field.Interface().(type) {
		case time.Time, *time.Time:
			if currentValue == "" {
				continue
			}

			timeFormat := ""
			switch tagInfo.TimeFormat {
			case timeFormatDatetime:
				timeFormat = "2006-01-02T15:04"
			case timeFormatDate:
				timeFormat = time.DateOnly
			case timeFormatTime:
				timeFormat = "15:04"
			case timeFormatWeek:
				parsedTime, err := parseWeekTime(currentValue)
				if err != nil {
					parseValueError.ParseError = err
					return nil, parseValueError
				}

				if field.Kind() == reflect.Pointer {
					field.Set(reflect.ValueOf(parsedTime))
				} else {
					field.Set(reflect.ValueOf(*parsedTime))
				}

				continue
			}

			parsedTime, err := time.Parse(timeFormat, currentValue)
			if err != nil {
				parseValueError.ParseError = err
				return nil, parseValueError
			}

			if field.Kind() == reflect.Pointer {
				field.Set(reflect.ValueOf(&parsedTime))
			} else {
				field.Set(reflect.ValueOf(parsedTime))
			}

			continue
		case *multipart.File:
			file, _, err := r.FormFile(tagInfo.Name)
			if err != nil {
				if errors.Is(err, http.ErrMissingFile) {
					continue
				}

				parseValueError.ParseError = err
				return nil, parseValueError
			}

			field.Set(reflect.ValueOf(&file))

			continue
		case multipart.FileHeader, *multipart.FileHeader:
			_, header, err := r.FormFile(tagInfo.Name)
			if err != nil {
				if errors.Is(err, http.ErrMissingFile) {
					continue
				}

				parseValueError.ParseError = err
				return nil, parseValueError
			}

			if field.Kind() == reflect.Pointer {
				field.Set(reflect.ValueOf(header))
			} else {
				field.Set(reflect.ValueOf(*header))
			}

			continue
		}

		return nil, ErrUnsupportedType{FieldName: fieldName}
	}

	return &form, nil
}
