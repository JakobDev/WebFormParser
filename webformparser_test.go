// Copyright 2023 JakobDev. All rights reserved.
// SPDX-License-Identifier: MIT

package webformparser_test

import (
	"bytes"
	"mime/multipart"
	"net/http"
	"testing"
	"time"

	webformparser "codeberg.org/JakobDev/WebFormParser"

	"github.com/stretchr/testify/assert"
)

func isTimeEqual(first, second time.Time) bool {
	return first.Year() == second.Year() && first.Month() == second.Month() && first.Day() == second.Day() && first.Hour() == second.Hour() && first.Minute() == second.Minute()
}

func addFormFile(w *multipart.Writer, field, filename, content string) error {
	f, err := w.CreateFormFile(field, filename)
	if err != nil {
		return err
	}

	_, err = f.Write([]byte(content))
	if err != nil {
		return err
	}

	return nil
}

func getFileContent(t *testing.T, file *multipart.File) string {
	buf := new(bytes.Buffer)

	_, err := buf.ReadFrom(*file)
	if !assert.NoError(t, err) {
		return ""
	}

	by := buf.Bytes()

	return string(by)
}

func getFileHeaderContent(t *testing.T, header *multipart.FileHeader) string {
	file, err := header.Open()
	if !assert.NoError(t, err) {
		return ""
	}

	return getFileContent(t, &file)
}

func TestStandardTypes(t *testing.T) {
	buf := new(bytes.Buffer)
	w := multipart.NewWriter(buf)
	w.WriteField("TestString", "Test123")
	w.WriteField("TestInt", "42")
	w.WriteField("TestInt8", "42")
	w.WriteField("TestInt32", "42")
	w.WriteField("TestInt64", "42")
	w.WriteField("TestUint", "42")
	w.WriteField("TestUint8", "42")
	w.WriteField("TestUint32", "42")
	w.WriteField("TestUint64", "42")
	w.WriteField("TestFloat32", "1.2")
	w.WriteField("TestFloat64", "1.2")
	w.WriteField("TestTrue", "on")
	w.WriteField("TestFalse", "off")
	w.WriteField("TestEmpty", "")
	w.Close()

	req, err := http.NewRequest("POST", "/", buf)
	if !assert.NoError(t, err) {
		return
	}

	req.Header.Set("Content-Type", w.FormDataContentType())

	type WebForm struct {
		TestString  string
		TestInt     int
		TestInt8    int8
		TestInt32   int32
		TestInt64   int64
		TestUint    uint
		TestUint8   uint8
		TestUint32  uint32
		TestUint64  uint64
		TestFloat32 float32
		TestFloat64 float64
		TestTrue    bool
		TestFalse   bool
		TestEmpty   bool
	}

	form, err := webformparser.ParseForm(WebForm{}, req)
	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(t, "Test123", form.TestString)
	assert.Equal(t, 42, form.TestInt)
	assert.Equal(t, int8(42), form.TestInt8)
	assert.Equal(t, int32(42), form.TestInt32)
	assert.Equal(t, int64(42), form.TestInt64)
	assert.Equal(t, uint(42), form.TestUint)
	assert.Equal(t, uint8(42), form.TestUint8)
	assert.Equal(t, uint32(42), form.TestUint32)
	assert.Equal(t, uint64(42), form.TestUint64)
	assert.Equal(t, float32(1.2), form.TestFloat32)
	assert.Equal(t, 1.2, form.TestFloat64)
	assert.True(t, form.TestTrue)
	assert.False(t, form.TestFalse)
	assert.False(t, form.TestEmpty)
}

func TestInvalidBool(t *testing.T) {
	buf := new(bytes.Buffer)
	w := multipart.NewWriter(buf)
	w.WriteField("TestBool", "invalid")
	w.Close()

	req, err := http.NewRequest("POST", "/", buf)
	if !assert.NoError(t, err) {
		return
	}

	req.Header.Set("Content-Type", w.FormDataContentType())

	type WebForm struct {
		TestBool bool
	}

	_, err = webformparser.ParseForm(WebForm{}, req)

	assert.Error(t, err)
	assert.True(t, webformparser.IsErrParseValue(err))
}

func TestTime(t *testing.T) {
	buf := new(bytes.Buffer)
	w := multipart.NewWriter(buf)
	w.WriteField("TestDatetime", "2017-06-01T08:30")
	w.WriteField("TestDate", "2018-01-01")
	w.WriteField("TestTime", "09:30")
	w.WriteField("TestWeek", "2017-W12")
	w.Close()

	req, err := http.NewRequest("POST", "/", buf)
	if !assert.NoError(t, err) {
		return
	}

	req.Header.Set("Content-Type", w.FormDataContentType())

	type WebForm struct {
		TestDatetime time.Time
		TestDate     time.Time `webformparser:"timeformat:date"`
		TestTime     time.Time `webformparser:"timeformat:time"`
		TestWeek     time.Time `webformparser:"timeformat:week"`
	}

	form, err := webformparser.ParseForm(WebForm{}, req)
	if !assert.NoError(t, err) {
		return
	}

	assert.True(t, isTimeEqual(time.Date(2017, 6, 1, 8, 30, 0, 0, time.Local), form.TestDatetime))
	assert.True(t, isTimeEqual(time.Date(2018, 1, 1, 0, 0, 0, 0, time.Local), form.TestDate))
	assert.True(t, isTimeEqual(time.Date(0, 1, 1, 9, 30, 0, 0, time.Local), form.TestTime))

	weekYear, weekNumber := form.TestWeek.ISOWeek()
	assert.Equal(t, 2017, weekYear)
	assert.Equal(t, 12, weekNumber)
}

func TestFile(t *testing.T) {
	buf := new(bytes.Buffer)
	w := multipart.NewWriter(buf)
	if !assert.NoError(t, addFormFile(w, "FileA", "NameA", "TestA")) {
		return
	}
	if !assert.NoError(t, addFormFile(w, "FileB", "NameB", "TestB")) {
		return
	}
	if !assert.NoError(t, addFormFile(w, "FileC", "NameC", "TestC")) {
		return
	}
	w.Close()

	req, err := http.NewRequest("POST", "/", buf)
	if !assert.NoError(t, err) {
		return
	}

	req.Header.Set("Content-Type", w.FormDataContentType())

	type WebForm struct {
		FileA *multipart.File
		FileB multipart.FileHeader
		FileC *multipart.FileHeader
		FileD *multipart.FileHeader
	}

	form, err := webformparser.ParseForm(WebForm{}, req)
	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(t, "TestA", getFileContent(t, form.FileA))

	assert.Equal(t, "NameB", form.FileB.Filename)
	assert.Equal(t, "TestB", getFileHeaderContent(t, &form.FileB))

	assert.Equal(t, "NameC", form.FileC.Filename)
	assert.Equal(t, "TestC", getFileHeaderContent(t, form.FileC))

	assert.Nil(t, form.FileD)
}

func TestIgnore(t *testing.T) {
	buf := new(bytes.Buffer)
	w := multipart.NewWriter(buf)
	w.WriteField("TestA", "Hello")
	w.WriteField("TestB", "World")
	w.Close()

	req, err := http.NewRequest("POST", "/", buf)
	if !assert.NoError(t, err) {
		return
	}

	req.Header.Set("Content-Type", w.FormDataContentType())

	type WebForm struct {
		Ignore    string `webformparser:"name:TestA ignore"`
		NotIgnore string `webformparser:"name:TestB"`
	}

	form, err := webformparser.ParseForm(WebForm{}, req)
	if !assert.NoError(t, err) {
		return
	}

	assert.Empty(t, form.Ignore)
	assert.Equal(t, "World", form.NotIgnore)
}

func TestCustomType(t *testing.T) {
	type myCustomType int

	buf := new(bytes.Buffer)
	w := multipart.NewWriter(buf)
	w.WriteField("MyType", "10")
	w.Close()

	req, err := http.NewRequest("POST", "/", buf)
	if !assert.NoError(t, err) {
		return
	}

	req.Header.Set("Content-Type", w.FormDataContentType())

	type WebForm struct {
		MyType myCustomType
	}

	form, err := webformparser.ParseForm(WebForm{}, req)
	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(t, myCustomType(10), form.MyType)
}

func TestUnsupportedType(t *testing.T) {
	type UnsupportedType struct{}

	buf := new(bytes.Buffer)
	w := multipart.NewWriter(buf)
	w.WriteField("Hello", "World")
	w.WriteField("Unsupported", "Test")
	w.Close()

	req, err := http.NewRequest("POST", "/", buf)
	if !assert.NoError(t, err) {
		return
	}

	req.Header.Set("Content-Type", w.FormDataContentType())

	type WebForm struct {
		Hello       string
		Unsupported UnsupportedType
	}

	_, err = webformparser.ParseForm(WebForm{}, req)

	if assert.True(t, webformparser.IsErrUnsupportedType(err)) {
		assert.Equal(t, "Unsupported", err.(webformparser.ErrUnsupportedType).FieldName)
	}
}
