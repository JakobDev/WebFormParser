# WebFormParser

### Parse HTML forms into a Go struct

With WebFromParser you can parse a HTML form that you get with a POST request into a Go struct.

## Features:
- Easy usage
- No runtime dependencies
- Lightweight
- Tested with unit tests
- OpenSource

## Usage
Given you have the following HTML form:
```html
<form>
    <input name="FieldA">
    <input name="FieldB" type="number">
    <input name="FieldC" type="checkbox">
</form>
```

You can parse it into a struct with:
```go
type MyForm struct {
    FieldA string
    FieldB int
    FieldC bool
}

func handleFormPost(w http.ResponseWriter, r *http.Request) {
    form, err := webformparser.ParseForm(MyForm{}, r)
    if err != nil {
        // handle error
    }

    fmt.Printf("Value for FieldA: %s\n", form.FieldA)
}
```

For a complete example take a look at the [Demo](demo).

## Suported types
WebFormParser currently supports the following types:
- `string`
- `bool`
- `int`
- `int8`
- `int32`
- `int64`
- `uint`
- `uint8`
- `uint32`
- `uint64`
- `float32`
- `float64`
- `time.Time`
- `*time.Time`
- `*multipart.File`
- `multipart.FileHeader`
- `*multipart.FileHeader`

## Tag
You can set options for a field with the `webformparser` tag. The different options are separated by spaces. Currently the following options are supported:
| Option       | Description |
|--------------|-----------|
| `name:<name>` | By default, WebFormParses uses the Name of the struct field to lookup the Field in the HTML form. If you use anther name in he HTMl form, you can set it here.      |
| `timeformat:<format>` | If you have a field of the type `time.Time`, you use this option to set the type of the field in the HTML form. Supported values are `datetime`, `date`, `time` and `week`. Default is `datetime`. |
| `ignore` | If you set this option, the field will be ignored by WebFormParser |
