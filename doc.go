// Copyright 2023 JakobDev. All rights reserved.
// SPDX-License-Identifier: MIT

/*
With WebFromParser you can parse a HTML form that you get with a POST request into a Go struct.
For more information take a look at https://codeberg.org/JakobDev/WebFromParser
*/
package webformparser
