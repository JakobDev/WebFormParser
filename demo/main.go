// Copyright 2023 JakobDev. All rights reserved.
// SPDX-License-Identifier: MIT

package main

import (
	"fmt"
	"mime/multipart"
	"net/http"
	"time"

	"codeberg.org/JakobDev/WebFormParser"

	_ "embed"
)

var (
	//go:embed "website.html"
	HtmlCode string
)

type MyForm struct {
	Textfield     string
	Textfield2    string `webformparser:"name:AnotherTextfield"`
	Checkbox      bool
	Number        int
	DecimalNumber float64
	Datetime      time.Time
	Date          time.Time  `webformparser:"timeformat:date"`
	Time          *time.Time `webformparser:"name:MyTime timeformat:time"`
	Week          time.Time  `webformparser:"timeformat:week"`
	File          *multipart.FileHeader
}

func postFunc(w http.ResponseWriter, r *http.Request) {
	form, err := webformparser.ParseForm(MyForm{}, r)
	if err != nil {
		fmt.Printf("Error while parsing form: %v\n", err)
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	fmt.Printf("Textfield: %s\n", form.Textfield)
	fmt.Printf("Textfield2: %s\n", form.Textfield2)
	fmt.Printf("Checkbox: %t\n", form.Checkbox)
	fmt.Printf("Datetime: %s\n", form.Datetime.Format(time.DateTime))
	fmt.Printf("Number: %d\n", form.Number)
	fmt.Printf("Decimal Number: %f\n", form.DecimalNumber)
	fmt.Printf("Date: %s\n", form.Date.Format(time.DateOnly))

	if form.Time != nil {
		fmt.Printf("Time: %s\n", form.Time.Format(time.TimeOnly))
	}

	weekYear, weekNumber := form.Week.ISOWeek()
	fmt.Printf("Week: %d-%d\n", weekYear, weekNumber)

	if form.File != nil {
		fmt.Printf("Filename: %s\n", form.File.Filename)
	}

	http.Redirect(w, r, "/", http.StatusSeeOther)
}

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, HtmlCode)
	})
	http.HandleFunc("/post", postFunc)

	fmt.Println("Starting server at localhost:3000")

	err := http.ListenAndServe(":3000", nil)
	if err != nil {
		fmt.Printf("Error ListenAndServe: %v\n", err)
	}
}
